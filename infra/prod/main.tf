terraform {
  required_providers {
    google = "4.10.0"
  }

  backend "gcs" {
    bucket = "ad_terraform_bucket"
    prefix = "terraform/state"
  }
}

provider "google" {
  project = "terraform-project-reunion"
  region  = "eu"
}

locals {
  project_id  = "terraform-project-reunion"
  environment = "dev"
}
